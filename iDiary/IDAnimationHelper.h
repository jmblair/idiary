//
//  RWAnimationHelper.h
//  RandomWords
//
//  Created by James Blair on 8/22/12.
//  Copyright (c) 2012 James Blair. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IDAnimationHelper : NSObject

// Basic animations
+ (void)bounceView:(UIView *)view;
+ (void)bounceView:(UIView *)view forDuration:(float)seconds;
+ (void)bounceViewTwice:(UIView *)view;
+ (void)bounceViewTwice:(UIView *)view forDuration:(float)seconds;
+ (void)spinViewClockwise:(UIView *)view;
+ (void)spinViewClockwise:(UIView *)view forDuration:(float)seconds;
+ (void)spinViewCounterclockwise:(UIView *)view;
+ (void)spinViewCounterclockwise:(UIView *)view forDuration:(float)seconds;

@end
