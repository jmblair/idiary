//
//  RWAnimationHelper.m
//  RandomWords
//
//  Created by James Blair on 8/22/12.
//  Copyright (c) 2012 James Blair. All rights reserved.
//

#import "IDAnimationHelper.h"
#import <QuartzCore/QuartzCore.h>

@implementation IDAnimationHelper

const float DEFAULT_DURATION = 0.5f;

typedef void (^AnimationBlock)(void);
typedef void (^CompletionBlock)(BOOL finished);

#pragma mark Simple Animations
+ (void)bounceView:(UIView *)view
{
    [self bounceView:view forDuration:DEFAULT_DURATION];
} // end bounceView method


+ (void)bounceView:(UIView *)view forDuration:(float)seconds
{
    // Define the animation blocks in normal order
    AnimationBlock makeSmall = ^(void) {
        view.transform = CGAffineTransformMakeScale(0.85f, 0.85f);
    };
    AnimationBlock makeLarge = ^(void) {
        view.transform = CGAffineTransformMakeScale(1.15f, 1.15f);
    };
    AnimationBlock restoreToOriginal = ^(void) {
        view.transform = CGAffineTransformIdentity;
    };
    
    // Define completion blocks in reverse order
    CompletionBlock shrinkBack = ^(BOOL finished) {
        [UIView animateWithDuration:(0.2f * seconds)
                         animations:restoreToOriginal
                         completion:nil];
    };
    CompletionBlock bounceLarge = ^(BOOL finished) {
        [UIView animateWithDuration:(0.2f * seconds)
                         animations:makeLarge
                         completion:shrinkBack];
    };
    
    // Start the animation
    [UIView animateWithDuration:(0.5f * seconds)
                     animations:makeSmall
                     completion:bounceLarge];
} // end bounceView:forDuration method


+ (void)bounceViewTwice:(UIView *)view
{
    [self bounceViewTwice:view forDuration:DEFAULT_DURATION];
} // end bounceViewTwice method


+ (void)bounceViewTwice:(UIView *)view forDuration:(float)seconds
{
    AnimationBlock makeSmall1 = ^(void) {
        view.transform = CGAffineTransformMakeScale(0.85f, 0.85f);
    };
    AnimationBlock makeLarge1 = ^(void) {
        view.transform = CGAffineTransformMakeScale(1.15f, 1.15f);
    };
    AnimationBlock makeSmall2 = ^(void) {
        view.transform = CGAffineTransformMakeScale(0.95f, 0.95f);
    };
    AnimationBlock makeLarge2 = ^(void) {
        view.transform = CGAffineTransformMakeScale(1.05f, 1.05f);
    };
    AnimationBlock restoreToOriginal = ^(void) {
        view.transform = CGAffineTransformIdentity;
    };
    CompletionBlock shrinkBack2 = ^(BOOL finished) {
        [UIView animateWithDuration:0.2f * seconds
                         animations:restoreToOriginal
                         completion:nil];
    };
    CompletionBlock bounceLarge2 = ^(BOOL finished) {
        [UIView animateWithDuration:0.2f * seconds
                         animations:makeLarge2
                         completion:shrinkBack2];
    };
    CompletionBlock shrinkBack1 = ^(BOOL finished) {
        [UIView animateWithDuration:0.2f * seconds
                         animations:makeSmall2
                         completion:bounceLarge2];
    };
    CompletionBlock bounceLarge1 = ^(BOOL finished) {
        [UIView animateWithDuration:0.2f * seconds
                         animations:makeLarge1
                         completion:shrinkBack1];
    };
    
    // Start the animation
    [UIView animateWithDuration:0.5f * seconds
                     animations:makeSmall1
                     completion:bounceLarge1];
} // end bounceViewTwice:forDuration method


+ (void)spinViewClockwise:(UIView *)view
{
    [self spinViewClockwise:view forDuration:DEFAULT_DURATION];
} // end spinViewClockwise method


+ (void)spinViewClockwise:(UIView *)view forDuration:(float)seconds
{
    CAKeyframeAnimation *rotationAnimation;
    rotationAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    rotationAnimation.values = @[[NSNumber numberWithFloat:0.0 * M_PI],
                                [NSNumber numberWithFloat:0.75 * M_PI],
                                [NSNumber numberWithFloat:1.5 * M_PI],
                                [NSNumber numberWithFloat:2.0 * M_PI]];
    rotationAnimation.calculationMode = kCAAnimationPaced;
    
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.fillMode = kCAFillModeForwards;
    
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    rotationAnimation.duration = seconds;
    
    CALayer *layer = [view layer];
    [layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
} // end spinViewClockwise:forDuration method


+ (void)spinViewCounterclockwise:(UIView *)view
{
    [self spinViewCounterclockwise:view forDuration:DEFAULT_DURATION];
} // end spinViewCounterclockwise method


+ (void)spinViewCounterclockwise:(UIView *)view forDuration:(float)seconds
{
    CAKeyframeAnimation *rotationAnimation;
    rotationAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    rotationAnimation.values = @[[NSNumber numberWithFloat:0.0 * M_PI],
                                [NSNumber numberWithFloat:-0.75 * M_PI],
                                [NSNumber numberWithFloat:-1.5 * M_PI],
                                [NSNumber numberWithFloat:-2.0 * M_PI]];
    rotationAnimation.calculationMode = kCAAnimationPaced;
    
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.fillMode = kCAFillModeForwards;
    
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    rotationAnimation.duration = seconds;
    
    CALayer *layer = [view layer];
    [layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
} // end spinViewCounterclockwise:forDuration method

@end
