//
//  IDAppDelegate.h
//  iDiary
//
//  Created by James Blair on 9/9/12.
//  Copyright (c) 2012 James Blair. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
