//
//  IDAppDelegate.m
//  iDiary
//
//  Created by James Blair on 9/9/12.
//  Copyright (c) 2012 James Blair. All rights reserved.
//

#import "IDAppDelegate.h"
#import "IDCaptureViewController.h"
#import "IDHistoryViewController.h"
#import "IDSettingsViewController.h"

@implementation IDAppDelegate

- (void)configureUIAppearance
{
    // Use UIAppearance proxy to set appearance
    // across-the-board for the entire app
    
    // TODO...
    NSLog(@"This is where we'll configure the UI appearance for the entire app");
#warning incomplete implementation
}

- (UIViewController *)configureRootViewController
{
    // Configure three UIViewControllers for the three tabs
    UIViewController * vc1 = [[IDCaptureViewController alloc] init];
    UIViewController * vc2 = [[IDHistoryViewController alloc] init];
    UIViewController * vc3 = [[IDSettingsViewController alloc] init];
    
    // Add each to a UINavigationController
    UINavigationController * nc1 =
    [[UINavigationController alloc] initWithRootViewController:vc1];
    UINavigationController * nc2 =
    [[UINavigationController alloc] initWithRootViewController:vc2];
    UINavigationController * nc3 =
    [[UINavigationController alloc] initWithRootViewController:vc3];
    
    // Now add the navigation contollers to a UITabBarController and return it
    NSArray * viewControllers = @[nc1, nc2, nc3];
    UITabBarController * tbc = [[UITabBarController alloc] init];
    [tbc setViewControllers:viewControllers];
    
    return tbc;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    [self.window setRootViewController:[self configureRootViewController]];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
