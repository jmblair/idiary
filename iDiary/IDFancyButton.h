//
//  IDFancyButton.h
//  iDiary
//
//  Created by James Blair on 9/9/12.
//  Copyright (c) 2012 James Blair. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

@interface IDFancyButton : UIButton
{
    SystemSoundID buttonSound;
}
- (void)configureButtonWithBackgroundColor:(UIColor *)color shiny:(BOOL)isShiny;
- (void)configureSoundEffectForFilePath:(NSString *)filePath;
@end
