//
//  IDFancyButton.m
//  iDiary
//
//  Created by James Blair on 9/9/12.
//  Copyright (c) 2012 James Blair. All rights reserved.
//

#import "IDFancyButton.h"
#import "IDAnimationHelper.h"
#import <QuartzCore/QuartzCore.h>

@implementation IDFancyButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    } // end if block
    return self;
}

- (void)configureButtonWithBackgroundColor:(UIColor *)color shiny:(BOOL)isShiny
{
    // Get the button layer and give it rounded corners
    // with a semi-transparent button
    CALayer *layer = self.layer;
    layer.cornerRadius = 8.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 4.0f;
    layer.borderColor = [UIColor colorWithWhite:0.4f alpha:0.2f].CGColor;
    
    // Create a shiny layer that goes on top of the button
    CAGradientLayer *shineLayer = [CAGradientLayer layer];
    shineLayer.frame = self.layer.bounds;
    // Set the gradient colors
    shineLayer.colors = @[(id)[UIColor colorWithWhite:1.0f alpha:0.35f].CGColor,
    (id)[UIColor colorWithWhite:1.0f alpha:0.15f].CGColor,
    (id)[UIColor colorWithWhite:0.75f alpha:0.15f].CGColor,
    (id)[UIColor colorWithWhite:0.4f alpha:0.15f].CGColor,
    (id)[UIColor colorWithWhite:1.0f alpha:0.35f].CGColor];
    // Set the relative positions of the gradient stops
    shineLayer.locations = @[@0.0f,
    @0.5f,
    @0.5f,
    @0.8f,
    @1.0f];
    
    // Add the layer to the button
    [self.layer addSublayer:shineLayer];
    
    [self setBackgroundColor:color];
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    AudioServicesPlaySystemSound(buttonSound);
    [IDAnimationHelper bounceView:self];
    [super touchesEnded:touches withEvent:event];
}

- (void)configureSoundEffectForFilePath:(NSString *)filePath
{
    NSString *soundFile = [[NSBundle mainBundle] pathForResource:filePath ofType:@"wav"];
    CFURLRef soundFileURL = (__bridge CFURLRef)[NSURL fileURLWithPath:soundFile];
    AudioServicesCreateSystemSoundID(soundFileURL, &buttonSound);
    //AudioServicesPlaySystemSound(buttonSound);
}

@end
