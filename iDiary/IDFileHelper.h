//
//  RWFileHelper.h
//  RandomWords
//
//  Created by James Blair on 8/23/12.
//  Copyright (c) 2012 James Blair. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IDFileHelper : NSObject
+ (NSString *)pathInDocumentDirectory:(NSString *)path;
+ (void)saveObject:(id<NSCoding>)object forKey:(NSString *)key atPath:(NSString *)path;
+ (id<NSCoding>)readObjectForKey:(NSString *)key atPath:(NSString *)path;
@end
