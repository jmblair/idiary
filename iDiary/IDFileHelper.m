//
//  RWFileHelper.m
//  RandomWords
//
//  Created by James Blair on 8/23/12.
//  Copyright (c) 2012 James Blair. All rights reserved.
//

#import "IDFileHelper.h"

@implementation IDFileHelper

+ (NSString *)pathInDocumentDirectory:(NSString *)path
{
    NSString *home = NSHomeDirectory();
    NSString *documents = [home stringByAppendingPathComponent:@"Documents"];
    return [documents stringByAppendingPathComponent:path];
}



+ (void)saveObject:(id)object forKey:(NSString *)key atPath:(NSString *)path
{
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:object forKey:key];
    [archiver finishEncoding];
    [data writeToFile:path atomically:YES];
}

+ (id)readObjectForKey:(NSString *)key atPath:(NSString *)path
{
    NSData *codedData = [[NSData alloc] initWithContentsOfFile:path];
    if (codedData == nil)
        return nil;
    
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:codedData];
    NSData *data = [unarchiver decodeObjectForKey:key];
    [unarchiver finishDecoding];
    return data;
}

@end
