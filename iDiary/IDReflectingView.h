//
//  RWReflectingView.h
//  RandomWords
//
//  Created by Erica Sadun -- iOS5 Developer's Cookbook
//  http://github.com/erica/iOS-5-Cookbook
//  Adapted by James Blair on 8/22/12.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface IDReflectingView : UIView
{
    CAGradientLayer *gradient;
}
- (void) setupReflection;
@property (nonatomic, assign) BOOL usesGradientOverlay;
@end