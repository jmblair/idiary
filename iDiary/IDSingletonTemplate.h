//
//  IDSingletonTemplate.h
//  iDiary
//
//  Created by James Blair on 9/9/12.
//  Copyright (c) 2012 James Blair. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IDSingletonTemplate : NSObject

+ (IDSingletonTemplate *)defaultTemplate;

@end
