//
//  IDSingletonTemplate.m
//  iDiary
//
//  Created by James Blair on 9/9/12.
//  Copyright (c) 2012 James Blair. All rights reserved.
//

#import "IDSingletonTemplate.h"

static IDSingletonTemplate * _defaultTemplate = nil;

@implementation IDSingletonTemplate

+ (IDSingletonTemplate *)defaultTemplate
{
    if (_defaultTemplate == nil) {
        _defaultTemplate = [[super allocWithZone:NULL] init];
    } // end if block
    return _defaultTemplate;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self defaultTemplate];
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        // Add any custom initialization here for the singleton instance
        
    } // end if block
    return self;
}

@end
