//
//  main.m
//  iDiary
//
//  Created by James Blair on 9/9/12.
//  Copyright (c) 2012 James Blair. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IDAppDelegate class]));
    }
}
